#!/bin/bash
#
# Artifacts from child pipelines aren't treated normally
# GitLab currently doesn't support any sane behavior
#
# See: https://gitlab.com/gitlab-org/gitlab/-/issues/215725#note_732899964
#
# Requires env: GITLAB_PRIVATE_TOKEN

set -o errexit -o pipefail -o noclobber -o nounset

API="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}"
AUTH_HEADER="PRIVATE-TOKEN: ${GITLAB_PRIVATE_TOKEN}"

# Fetch the IDs of all child pipelines
curl -sS --header "${AUTH_HEADER}" "${API}/pipelines/${CI_PIPELINE_ID}/bridges" |
	jq ".[].downstream_pipeline.id" |
	# Fetch the IDs of their "build:*" jobs that completed successfully
	xargs -i curl -sS --header "${AUTH_HEADER}" "${API}/pipelines/{}/jobs?scope=success" |
	jq -r '.[] | select((.name | startswith("build:")) and .artifacts_file != null) | (.id|tostring) + "-" + (.name|split(":")[2])' |
	# Fetch the artifacts uploaded by these jobs
	while IFS='-' read job dist arch; do
		dest=artifacts-$job.zip
		curl -sSL --header "${AUTH_HEADER}" --output $dest "${API}/jobs/$job/artifacts"
		mkdir -p $dist
		unzip -o $dest -d $dist/
		rm $dest
	done

