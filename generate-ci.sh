#!/bin/bash

set -eu -o pipefail
unset CDPATH

parse_package_release() {
	local package="$1"

	[ ! -d ./$package ] && { echo "Error: unknown package '$package'" >&2; return; }

	cd $package/
	ls -1 release-* | while IFS=- read _ arch; do
		source release-$arch

		for tag in ${dists[@]}; do
			IFS=@ read dist image <<< $tag
			generate_job_spec
		done
	done
	cd ..
}

generate_job_spec() {
	local before_script
	local fetch_command
	
	case $dist in
		bionic|bullseye|buster|focal|xenial)
			IFS= read -r -d '' before_script <<-EOF || true
			    - apt-get update
			    - DEBIAN_FRONTEND=noninteractive apt-get install -y debhelper dh-exec
			EOF
			;;
	esac

	# Source kind detection, supported options:
	# http:// or https://
	# git:<uri>
	# gitmodule
	# <none> source is contained in the directory
	if [ -n "${source-}" ]; then
		if [[ $source == http* ]]; then
			before_script+="    - apt-get install -y wget unzip"$'\n'
			fetch_command="    - SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt wget -O source.bin ${source}"
			local default_fetch_command_args="--strip-components=1"

			if [ -n "${unpack_command-}" ]; then
				fetch_command+=$'\n'"    - ${unpack_command}"
			else
				# Archive type detection
				local archive_filename=$( basename $source )
			
				# .tar.gz
				if [ $archive_filename != $( basename $archive_filename .tar.gz ) ]; then
					fetch_command+=$'\n'"    - tar xvzf source.bin ${fetch_command_args:-$default_fetch_command_args} -C ${package}/"
				fi
				if [ $archive_filename != $( basename $archive_filename .zip ) ]; then
					fetch_command+=$'\n'"    - unzip source.bin -d ${package}/"
				fi
				# Insert other extension support here
			fi
		elif [[ $source == git* ]]; then
			before_script+="    - apt-get install -y git"$'\n'

			# TODO: url support
			if [ $source = gitmodule ]; then
				fetch_command="    - git submodule update --init --recursive"
			fi
		fi
	fi

	cat <<-EOF
	build:${package}:${dist}-${arch}:
	  stage: build
	  rules:
	    - if: '\$CI_COMMIT_TAG =~ /^${package}@/'
	  artifacts:
	    name: '${package}-\$CI_JOB_ID'
	    paths:
	      - ${package}_*_${arch}.deb

	  image: ${image}
	  before_script:
	${before_script}
	  script:
	${fetch_command-}
	    - cd ${package}/
	    - dpkg-buildpackage -b

	EOF
}

# Tag was passed, just one package
if [ -n "${1-}" ]; then
	IFS=@ read package _ <<< $1

	parse_package_release $package
	exit 0
fi

# No tag was passed, all packages
cat packages.list | egrep -v '^#|^$' | while read package; do
	parse_package_release $package
done
