import traceback
import re

from bs4 import BeautifulSoup
from couchpotato.core.logger import CPLog
from couchpotato.core.media._base.providers.torrent.base import TorrentProvider


log = CPLog(__name__)


class Base(TorrentProvider):

    urls = {
        'test': 'https://tracker.czech-server.com/',
        'login': 'https://tracker.czech-server.com/prihlasenie.php',
        'login_check': 'https://tracker.czech-server.com/torrents.php',
        'detail': 'https://tracker.czech-server.com/details.php?id=%s',
        'search': 'https://tracker.czech-server.com/torrents.php',
        'download': 'https://tracker.czech-server.com/download.php?id=%s&f=%s',
    }

    cat_ids = [
        ([31, 33], ['720p', '1080p', 'bd50']),
        ([4, 36], ['brrip', 'dvdrip']),
        ([1, 2], ['dvdr']),
    ]

    http_time_between_calls = 1  # Seconds
    login_fail_msg = 'Username or password incorrect'
    cat_backup_id = None

    def _searchOnTitle(self, title, movie, quality, results):

        post_data = {
            'search': '%s %s' % (title.replace(':', ''), movie['info']['year']),
            'active': 1,
            'kontodosl': 1,
        }
        post_data.update({'zcat%d' % id: 1 for id in self.getCatId(quality)}) 
        data = self.getHTMLData(self.urls['search'], data=post_data)

        if data:
            html = BeautifulSoup(data)

            try:
                result_table = html.find(attrs = {'id': 'torrenty_tabulka'})

                if not result_table:
                    log.error('failed to generate result_table')
                    return

                entries = result_table.find_all('tr', attrs={'class', 'torrenty_lista'})

                for result in entries:
                    cells = result.find_all('td')
                    link = result.find('a', attrs = {'href': lambda x: x and x.startswith('download.php?')})
                    torrent_id = link['href'].replace('download.php?id=','').split('&f=')[0]
                    torrent_file = link['href'].replace('download.php?id=','').split('&f=')[1]
                    size = self.parseSize(cells[6].contents[0])
                    name = cells[1].find('a')['title'].replace('Detaily: ', '')
                    seeders_row = cells[7].contents[0]
                    seeders = seeders_row.getText()
                    leechers_row = cells[8].contents[0]
                    leechers = leechers_row.getText()

                    results.append({
                        'id': torrent_id,
                        'name': name,
                        'url': self.urls['download']  % (torrent_id,torrent_file),
                        'detail_url': self.urls['detail'] % torrent_id,
                        'size': size,
                        'seeders': seeders,
                        'leechers': leechers, 
                    })

            except:
                log.error('Failed to parsing %s: %s', (self.getName(), traceback.format_exc()))

    def getLoginParams(self):
        return {
            'uid': self.conf('username'),
            'pwd': self.conf('password'),
        }

    def loginSuccess(self, output):
        return 'errornot_authorized' not in output.lower()

    loginCheckSuccess = loginSuccess


config = [{
    'name': 'trezzor',
    'groups': [
        {
            'tab': 'searcher',
            'list': 'torrent_providers',
            'name': 'Trezzor',
            'description': '<a href="https://tracker.czech-server.com" target="_blank">Trezzor</a>',
            'wizard': True,
            'icon': 'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAB5lBMVEX////u7u3i5OXb29vT1NXMz8/Jy8zGxcXAwMC3t7fx8vLm5uYxQEdicnhufoVzhYx0hY1xhI5vho90jJd1i5Vwh46PoaW2xMr3+fk9T1d8nKh0jZlvgZCVn5ijp56zsJ+1rJm7u7OVoqZsiphGX2n8/f4+TVNdgpaKbEv41WpRTy1XXUllfImOr72YusWRsL17k5pkfYg6S1FVe4yukF/a0ZYAARBJYW6HprSVoaWWrbKavMZwipaDm6M9TVRMcIOcgFf455UOGypif4yVs8Bld4Cdu8OYnZ53lqSbsLlIV15BX3B8XDj05pMbMUZtbm57k5yau8iXt8NXcHqJqLOjtLtaZmslOEE5JBX/44kmPVI+U12GpbFxdXYtMjOAl54/T1asvsRmcXYrPkgyIRb/23ghOFBgfopYX2J8h4paanBEYm2vvMFveX0rPkYvHhP/9LwbMUBCWWRodHZZbHQ8QkNne4M7WGavubuCi44mN0AgGRT/4JAVKjxAVl8VFBRSXV9UYWdCW2iwtLb19vcSJCoWDQj/0oQRJDE4SlAzRUwpNz48UVs1SlMDERW3urlIUlcGAABwWDogKzAnNDkkOUAmOkJgbHBbYGKTlZfw8fLLzc9ucXOxtLkAAAAsNTmgp6rS1NIsgtyjAAAAAWJLR0QAiAUdSAAAAAd0SU1FB90GHhAsLT065PYAAADNSURBVBjTY2DABIxMzCysbOwcnFwQPjcPLx+/gKCQsIiomDhIQEJSSlpGVk5eQVFJWQUkoKqmrqGppa2jq6dvYAjWY2RsYmpmbmFpZW1jCxaws3dwdHJ2cXVz9/AEC3h5+/j6+QcEBgWHhIIFwsIjIqOiY2Lj4hMSwQJJySmpaekZmYZZ2Tlggdy8/ILCouKS0rLyCrBAZVV1TW1dfUNjU3QzWKClta29o7Oru6e3rx/i+AkTJ02eMnXa9BkzZ0EEZs+ZO2/+AlQfL4SzAJqrM37HHJC/AAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE3LTA3LTI2VDE5OjUzOjMzKzAyOjAwR6c6zQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxMy0wNi0zMFQxNjo0NDo0NSswMjowMAS5HiEAAAAASUVORK5CYII=',
            'options': [
                {
                    'name': 'enabled',
                    'type': 'enabler',
                    'default': False,
                },
                {
                    'name': 'username',
                    'default': '',
                },
                {
                    'name': 'password',
                    'default': '',
                    'type': 'password',
                },
                {
                    'name': 'seed_ratio',
                    'label': 'Seed ratio',
                    'type': 'float',
                    'default': 1,
                    'description': 'Will not be (re)moved until this seed ratio is met.',
                },
                {
                    'name': 'seed_time',
                    'label': 'Seed time',
                    'type': 'int',
                    'default': 40,
                    'description': 'Will not be (re)moved until this seed time (in hours) is met.',
                },
                {
                    'name': 'extra_score',
                    'advanced': True,
                    'label': 'Extra Score',
                    'type': 'int',
                    'default': 20,
                    'description': 'Starting score for each release found via this provider.',
                }
            ],
        },
    ],
}]
